(ns dns-sd.core
  (:gen-class)
  (:require [clojure.java.io])
  (:import javax.jmdns.JmDNS)
  (:import javax.jmdns.ServiceEvent)
  (:import javax.jmdns.ServiceListener)
  (:import javax.jmdns.ServiceInfo)
  (:import java.net.InetAddress))


(def SampleListener (reify ServiceListener
         (serviceAdded [this event] (println (str "Service added!" (.getInfo event))))
         (serviceRemoved [this event] (println (str" Service removed" (.getInfo event))))
         (serviceResolved [this event] (println (str" Service resolved" (.getInfo event))))))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!")
  (let [localHost (InetAddress/getLocalHost)
        jmdns (JmDNS/create localHost) ]
       (.addServiceListener jmdns "_http._tcp.local." SampleListener)
       (.registerService jmdns (ServiceInfo/create "_mqtt._tcp.local." "raspberryMqtt" 8098 ""))
       (println "fim")
       )
  )
